%bcond_with bootstrap

Name:           plexus-xml
Version:        3.0.0
Release:        2%{?dist}
Summary:        Plexus XML Utilities
# Licensing breakdown:
# Apache-1.1: src/main/java/org/codehaus/plexus/util/xml/StringUtils.java
# xpp: src/main/java/org/codehaus/plexus/util/xml/pull/MXParser.java
# Everything else is Apache-2.0
License:        Apache-1.1 AND Apache-2.0 AND xpp
URL:            https://codehaus-plexus.github.io/plexus-xml/
BuildArch:      noarch
ExclusiveArch:  %{java_arches} noarch

Source0:        https://github.com/codehaus-plexus/%{name}/archive/%{name}-%{version}.tar.gz

%if %{with bootstrap}
BuildRequires:  javapackages-bootstrap
%else
BuildRequires:  maven-local
BuildRequires:  mvn(org.codehaus.plexus:plexus:pom:)
%endif

%description
A collection of various utility classes to ease working with XML.

%{?javadoc_package}

%prep
%setup -q -n %{name}-%{name}-%{version}

%build
# Test dependencies are not packaged
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%doc README.md
%license NOTICE.txt LICENSE.txt

%changelog
* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 3.0.0-2
- Bump release for June 2024 mass rebuild

* Fri Feb  2 2024 Mikolaj Izdebski <mizdebsk@redhat.com> - 3.0.0-1
- Initial packaging
